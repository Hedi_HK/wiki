(function() {
	
	$("#btn-createuser").on("click",function() {
		createUser();
	});

	$("#btn-allUsers").on("click",function() {
		getAllUsers();
	});
	$("#btn-edituser").on("click",function() {
		updateUser();
		$("#edituserModal").hide();
		$('.modal-backdrop').remove();
	});
	
	
})();

function makeUserTable(myArray) {
    var result = "";
    for(let i=0; i<myArray.length; i++) {
        result += "<tr>";
        result += "<td>"+myArray[i].id+"</td>";
        result += "<td>"+myArray[i].username+"</td>";
        result += "<td>"+myArray[i].password+"</td>";
        result += "<td>"+myArray[i].email+"</td>";
        result += "<td>"+myArray[i].nom+"</td>";
        result += "<td>"+myArray[i].prenom+"</td>";
        result += "<td>"+myArray[i].role+"</td>";
        result += "<td>"+myArray[i].userStatus+"</td>";
        result += "<td>"+myArray[i].civilite+"</td>";
        result += "<td><button type='button' id='btn-activateUser" +myArray[i].id+"'  class='btn btn-success useractivator'>Activate</button>" +" "+
        		  "<button type='button' id='btn-deactivateUser"+ myArray[i].id+"'class=\"btn userdeactivator btn-warning\">Deactivate</button>" +" "+
        		  "<button type='button' id='btn-deleteUser" +  myArray[i].id+"'class=\"btn userdeleter btn-danger\">Delete</button></td>";
        result += "<td><button type='button' id='btn-editUser" +myArray[i].id+"'  class='btn btn-primary usereditor'>Edit</button></td>"
        result += "</tr>";

    }
    result += "</table>";
   
    return result;
}

function createUser(){
	var formElements=document.getElementById("register-form").elements;
	username = formElements.namedItem("username").value;
	nom = formElements.namedItem("nom").value;
	prenom = formElements.namedItem("prenom").value;
	email = formElements.namedItem("email").value;
	password = formElements.namedItem("password").value;
	civilite = formElements.namedItem("civilite").value;

	$.ajax({
		type : "POST",
		url : "doCreateUser",
		dataType : "json",
		data : {
			username:username,
			nom:nom,
			prenom:prenom,
			email:email,
			password:password,
			civilite:civilite
		}
	}).done(function(res) {
		if(res.sessionMap.user!=null){
			console.log("ajax post register ok");
			$("#btn-logout,#btn-allUsers,#btn-allArticles,#btn-importxml,#btn-newArticle").removeClass("hidden");
			$("#btn-loginModal,#btn-registerModal").hide();
			$("#registerModal").hide();
			$('.modal-backdrop').remove();
		}
		else{
			document.querySelector("#registerError").classList.remove("hidden");
		}
	})
}

function getAllUsers(){
	$.ajax({
		type : "GET",
		url : "doGetAllUsers",
		dataType : "json",
		data : {}
	}).done(function(res) {
				$("#mainContainer").load("pages/userList.jsp #mainContainer > *",function(){
					$("#userTable").append(makeUserTable(res.userList));
				    $(".useractivator").on("click",function() {
				    	updateUserStatus(this.id.substring(16),"ACTIVE")
					});
				    $(".userdeactivator").on("click",function() {
				    	updateUserStatus(this.id.substring(18),"INACTIVE")
					});
				    $(".userdeleter").on("click",function() {
				    	updateUserStatus(this.id.substring(14),"SUSPENDED")
					});
				    $(".usereditor").on("click",function() {
				    	$("#edituserModal").modal('toggle');
				    	var id = parseInt(this.id.substring(12));
				    	var result = $.grep(res.userList, function(e){ return e.id === id; });
				    	fillForm(result[0]);
					});
				});
		})
	
}

function updateUser(){
	var formElements=document.getElementById("edituserform").elements;
	id = formElements.namedItem("id").value;
	username = formElements.namedItem("username").value;
	nom = formElements.namedItem("nom").value;
	prenom = formElements.namedItem("prenom").value;
	email = formElements.namedItem("email").value;
	password = formElements.namedItem("password").value;
	civilite = formElements.namedItem("civilite").value;
	userStatus = formElements.namedItem("userStatus").value;
	role = formElements.namedItem("role").value;

	$.ajax({
		type : "POST",
		url : "doUpdateUser",
		dataType : "json",
		data : {
				username:username,
				nom:nom,
				prenom:prenom,
				email:email,
				password:password,
				civilite:civilite,
				userStatus:userStatus,
				role:role,
				id:id
				}
	}).done(function() {
			getAllUsers();
		})
}

function updateUserStatus(id,userStatus){
	$.ajax({
		type : "POST",
		url : "doUpdateUserStatus",
		dataType : "json",
		data : {
				id:id,
				userStatus:userStatus,
				}
	}).done(function() {
			getAllUsers();
		})
}

function fillForm(user){
	console.log("filling form with: " + user)
	var formElements=document.getElementById("edituserform").elements;
	formElements.namedItem("id").value=user.id;
	formElements.namedItem("username").value=user.username;
	formElements.namedItem("nom").value=user.nom
	formElements.namedItem("prenom").value=user.prenom;
	formElements.namedItem("email").value=user.email;
	formElements.namedItem("password").value=user.password;
	formElements.namedItem("civilite").value=user.civilite;
}