(function() {

	$("#btn-createarticle").on("click",function() {
		createArticle();
	});
	
	$("#btn-allArticles").on("click",function() {
		getAllArticles();
	});
	
	$("#btn-importxml").on("change",function() {
		uploadArticle();
	});
	

})();

function createArticle(){
	var formElements=document.getElementById("article-form").elements;
	title = formElements.namedItem("title").value;
	content = formElements.namedItem("content").value;
	author = user;
	$.ajax({
		type : "POST",
		url : "doCreateArticle",
		dataType : "json",
		data : {
				title:title,
				user:user,
				content:content
				}
	}).done(function() {
			console.log("article post ok");
		})
}

function getAllArticles(){
	$.ajax({
		type : "POST",
		url : "doGetAllArticles",
		dataType : "json",
		data : {}
	}).done(function(res) {
		$("#mainContainer").load("pages/articleTemplate.jsp #mainContainer > *",function(){
			$("#articleList").append(showArticles(res.articleList));
		})
		})
}

function showArticles(myArray) {
	var result = "";
	for(let i=0; i<myArray.length; i++) {
		result +="<div class='panel-body'>"
		result += "<h3>"+myArray[i].title+"</h3>";
		result += "<p>"+myArray[i].content+"</p>";
		result +="</div>"
		
	}
	return result;
}

function uploadArticle(){
	var formData = new FormData();
	var myfile = event.target.files[0];
	console.log(myfile);
	formData.append("file", myfile);
	$.ajax({
		  url: "doCreateArticleFromXML",
		  type: "POST",
		  data: formData,
		  processData: false,  
		  contentType: false  
		});
}