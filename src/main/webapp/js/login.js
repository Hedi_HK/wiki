(function() {
	$("#btn-login").on('click',login);
	$("#btn-logout").on('click',logout);
	checkLogin();
})();


function logout() {
	
	$.ajax({
		type : "GET",
		url : "doLogout",
		dataType : "json",
		data : {}
	}).done(function(res) {
		
		console.log("signing out");
		location.reload();
	})
}

 function login(){
	var username = document.getElementById("login-form").elements.namedItem("username").value;
	var password = document.getElementById("login-form").elements.namedItem("password").value;
	$.ajax({
		type : "POST",
		url : "doLogin",
		dataType : "json",
		data : {
			username : username,
			password : password
		}
	})
	.done(function(res) {
				if (res.sessionMap.user != null) {
					$("#btn-logout,#btn-allUsers,#btn-allArticles,#btn-importxml,#btn-newArticle").removeClass("hidden");
					$("#btn-loginModal,#btn-registerModal").hide();
					$("#loginModal").hide();
					$('.modal-backdrop').remove();
					console.log("logged as "+ res.sessionMap.user.username);
				} else {
					console.log("login error");
					document.querySelector("#login-form").reset();
					document.querySelector("#loginError").classList.remove("hidden");
				}
			})
	
}
 function checkLogin(){
		if (!user){
			console.log("no one connected");
		}
		else{
			console.log(user.username + " is connected");
			$("#btn-logout,#btn-allUsers,#btn-allArticles,#btn-importxml,#btn-newArticle").removeClass("hidden");
			$("#btn-loginModal,#btn-registerModal").hide();
		}
		document.body.style.visibility = "visible";
 }
 