<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	import="com.perou.wiki.beans.Civilite,com.perou.wiki.beans.UserStatus"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h1 class="modal-title">New Article</h1>
		</div>
		<div class="modal-body">
			<form id="article-form">
				<div class="form-group">
					Title: <input type="text" name="title">
					<textarea name="content" rows="4" cols="50">
					Enter article content.
					</textarea>
					<button type="button" id="btn-createarticle" class="btn btn-success">Save Article</button>
				</div>
			</form>
		</div>

	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	</div>
</div>

</body>
</html>