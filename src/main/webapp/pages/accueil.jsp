<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script defer src="js/login.js"></script>
	<script defer src="js/user.js"></script>
	<script defer src="js/article.js"></script>
	<script defer src="js/comment.js"></script>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Wiki CGI/M2I</title>
</head>
<body style="visibility:hidden">

	<jsp:include page="navbar.jsp"></jsp:include>
	
	<div id="mainContainer" class="container">
		<h3>Bienvenue sur le Wiki CGI/M2I</h3>
	</div>

	<script type="text/javascript">
		var user = "<c:out value="${user}"/>";
	</script>	
	
</body>
</html>
