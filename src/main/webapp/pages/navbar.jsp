<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
import="com.perou.wiki.beans.Search"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">	
			<ul class="nav navbar-nav">
					<a class="navbar-brand" href="#">Wiki CGI/M2I</a>
					<li><button class="btn-link btn hidden" type="button" id="btn-allUsers"><b>List all users</b></button></li>
					<li><button class="btn-link btn hidden" type="button" id="btn-allArticles"><b>List all articles</b></button></li>
					<li><button class="btn-link btn hidden" type="button" data-toggle="modal" data-target="#newarticleModal" id="btn-newArticle"><b>New Article</b></button></li>
					<li><button class="btn-link btn hidden" type="button" id="btn-logout" ><b>Sign Out</b></button></li>
					<li><button class="btn-link btn" type="button" id="btn-loginModal" data-toggle="modal" data-target="#loginModal"><b>Login</b></button></li>
					<li><button class="btn-link btn" type="button" id="btn-registerModal" data-toggle="modal" data-target="#registerModal"><b>Register</b></button></li>
					<li><button class="btn-link btn" type="button" data-toggle="modal" data-target="#newcommentModal" id="btn-newComment"><b>New Comment</b></button></li>
					<li>
	        			<div class="input-group">
	       					<input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
	       				</div>
	       			</li>
	            	<li>
	            		<select class="form-control" name="searchType">
	            			<c:set var="enumValues" value="<%=Search.values()%>"/>
	            			<c:forEach items="${enumValues}" var="enumValue">
								<option value="${enumValue}"> ${enumValue} </option>
							</c:forEach>	            				
	            		</select>
            		</li>
            		<li><button class="btn btn-info" type="button" id="btn-search"><i class="glyphicon glyphicon-search"></i></button></li>	
	            	<li>
	            		<form enctype="multipart/form-data">	
	            		<label id="btn-importxml" class="btn btn-default btn-file hidden">
						    Import XML <input type="file" name="file" style="display: none;">
						</label>
						</form>
					</li>
			</ul>
		</div>
	</nav>
	<div id="loginModal" class="modal fade" role="dialog">
		<jsp:include page="connect/login.inc.jsp"></jsp:include>
	</div>
	<div id="registerModal" class="modal fade" role="dialog">
		<jsp:include page="connect/register.inc.jsp"></jsp:include>
	</div>
	<div id="edituserModal" class="modal fade" role="dialog">
		<jsp:include page="connect/edituserform.jsp"></jsp:include>
	</div>
	<div id="newarticleModal" class="modal fade" role="dialog">
		<jsp:include page="articleForm.jsp"></jsp:include>
	</div>
		<div id="newcommentModal" class="modal fade" role="dialog">
		<jsp:include page="commentForm.jsp"></jsp:include>
	</div>
</body>