<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h1 class="modal-title">New Comment</h1>
		</div>
		<div class="modal-body">
			<form id="comment-form">
				<div class="form-group">
					<textarea name="content" rows="4" cols="75"></textarea>
					<button type="button" id="btn-createcomment" class="btn btn-success">Save Comment</button>
				</div>
			</form>
		</div>

	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	</div>
</div>

</body>
</html>