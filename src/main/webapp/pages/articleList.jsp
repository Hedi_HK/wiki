<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>	
	<html>
	
		<head>
			 <link rel="stylesheet" href="css/bootstrap.min.css">
			<meta http-equiv="Content-Type" content="text/html;">
			<title>Wiki CGI/M2I</title>
		</head>
		
		<jsp:include page="navbar.jsp"></jsp:include>
		
		<body>
			<div class="container-fluid">
				<h2>Wiki Articles</h2>
				<table class="table table-hover table-bordered" data-show-toggle="true">
					<tr class="info">
						<th>Id</th>
						<th>Title</th>
						<th>Content</th>
						<th>Author</th>
						<th>Date</th>
						<th>Status</th>
						<th>Score</th>
						<th>Change article status</th>
					</tr>
				<c:forEach items="${articleList}" var="entry">
						<tr class="
						<c:choose>
						    <c:when test="${entry.value.articleStatus == 'VALIDATED'}">
						       success
						    </c:when>
						    <c:when test="${entry.value.articleStatus == 'PENDING'}">
						        warning
						    </c:when>
						    <c:when test="${entry.value.articleStatus == 'REFUSED'}">
						        danger
						    </c:when>
						    <c:otherwise>
						    </c:otherwise>
						</c:choose>
						">
						<th>${entry.value.id}</th>
						<th>${entry.value.title}</th>
						<th>${entry.value.content}</th>
						<th>${entry.value.author}</th>
						<th>${entry.value.datePublication}</th>
						<th>${entry.value.articleStatus}</th>
						<th>${entry.value.score}</th>
						
						
						
				</c:forEach>
				</table>
			</div>
		</body>
		<script src="js/bootstrap.min.js"></script>
	</html>