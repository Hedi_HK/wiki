<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	import="com.perou.wiki.beans.Civilite,com.perou.wiki.beans.UserStatus"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h1 class="modal-title">Register</h1>
		</div>
		<div class="modal-body">
		<div class="alert alert-danger hidden" role="alert" id="registerError">Creation compte impossible, email deja utilise</div>
			<form id="register-form">
				<div class="form-group">

					<label>Civilit�:</label>
						<c:set var="enumValues" value="<%=Civilite.values()%>"/> 
						<select class="form-control" name="civilite">
							<c:forEach items="${enumValues}" var="enumValue">
								<option value="${enumValue}">${enumValue}</option>
							</c:forEach>
						</select> 
					Login: <input class="form-control invalid warning" type="text" id="registerUsername" name="username" required>
					Nom: <input class="form-control" type="text" name="nom" required> 
					Pr�nom: <input class="form-control" type="text" name="prenom" required> 
					Adresse e-mail: <input class="form-control" type="email" name="email" required>
					Password: <input class="form-control" type="password" name="password" required> 
					<button type="button" id="btn-createuser" class="btn btn-success">Log In</button>
				</div>
			</form>
		</div>

	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	</div>
</div>

</body>
</html>