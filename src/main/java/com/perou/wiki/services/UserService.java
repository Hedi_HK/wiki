package com.perou.wiki.services;

import com.perou.wiki.beans.User;
import com.perou.wiki.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("userService")
public class UserService implements IUserService {
    @Autowired
    User user;

    @Autowired
    UserDao userDao;

    @Override
    public void createUser(User user) {
        userDao.create(user);
    }

    @Override
    public User readUser(Long id) {
        userDao.read(User.class, id);
        return user;
    }

    @Override
    public void updateUser(User user) {
        userDao.update(user);
    }

    @Override
    public void deleteUser(Long id) {
        userDao.delete(id);
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    @Override
    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }
}
