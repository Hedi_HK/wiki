package com.perou.wiki.services;

import com.perou.wiki.beans.User;

import java.util.List;

/**
 * Created by Administrateur on 20/09/2016.
 */
interface IUserService {
    void createUser(User user);
    User readUser(Long id);
    void updateUser(User user);
    void deleteUser(Long id);
    List<User> getAllUsers();
    User findByUsername(String username);

}
