package com.perou.wiki.dao;

import com.perou.wiki.beans.Article;

import java.util.List;

/**
 * Created by Administrateur on 20/09/2016.
 */
public interface IArticleDao extends IGenericDao{
    List<Article> getAllArticles();
}
