package com.perou.wiki.dao;

import com.perou.wiki.beans.Article;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrateur on 20/09/2016.
 */
@Repository("articleDao")
@Transactional(propagation = Propagation.REQUIRED)
public class ArticleDao extends GenericDao implements IArticleDao {

    private static String SELECT_QUERY="SELECT a FROM Article a";
    @Override
    public List<Article> getAllArticles() {
        return super.em.createQuery(SELECT_QUERY).getResultList();
    }
}
