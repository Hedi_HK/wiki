package com.perou.wiki.dao;

import com.perou.wiki.beans.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrateur on 20/09/2016.
 */
@Repository("userDao")
@Transactional(propagation = Propagation.REQUIRED)
public class UserDao extends GenericDao implements IUserDao{
    private static String SELECT_QUERY="SELECT u FROM User u";

    public List<User> getAllUsers() {
        return super.em.createQuery(SELECT_QUERY).getResultList();
    }

    public User findByUsername(String username) {
        return (User) super.em.createNamedQuery("User.findByName").setParameter("username",username).getResultList().get(0);
    }
}
