package com.perou.wiki.dao;

/**
 * Created by Lam on 20/09/2016.
 */
public interface IGenericDao  {

    /** Persist the newInstance object into database */
    void create(Object newInstance);

    /** Retrieve an object that was previously persisted to the database using
     *   the indicated id as primary key
     */
    Object read(Class className,Long id);

    /** Save changes made to a persistent object.  */
    void update(Object transientObject);

    /** Remove an object from persistent storage in the database */
    void delete(Object persistentObject);
}
