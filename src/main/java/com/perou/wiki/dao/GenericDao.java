package com.perou.wiki.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Administrateur on 20/09/2016.
 */
public abstract class GenericDao implements IGenericDao {

    @PersistenceContext
    protected EntityManager em;


    @Transactional
    public void create(Object newInstance) {
        em.persist(newInstance);
    }
    @Transactional
    public Object read(Class className, Long id) {
        return em.find(className, id);

    }
    @Transactional
    public void update(Object transientObject) {
        em.merge(transientObject);
    }
    @Transactional
    public void delete(Object persistentObject) {
        em.remove(persistentObject);
    }
}
