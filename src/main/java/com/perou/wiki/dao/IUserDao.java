package com.perou.wiki.dao;

import com.perou.wiki.beans.User;

import java.util.List;

/**
 * Created by Administrateur on 20/09/2016.
 */
public interface IUserDao extends IGenericDao {
    List<User> getAllUsers();
    User findByUsername(String username);
}
