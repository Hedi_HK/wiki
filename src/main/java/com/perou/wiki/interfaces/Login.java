package com.perou.wiki.interfaces;

@FunctionalInterface
public interface Login<T,T2,TR> {
	 TR login(T t,T2 t2);
}
