package com.perou.wiki.controllers.struts;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.perou.wiki.beans.Civilite;
import com.perou.wiki.beans.Role;
import com.perou.wiki.beans.User;
import com.perou.wiki.beans.UserStatus;

@SuppressWarnings("serial")
@ParentPackage("json-default")
public class UserAction extends ActionSupport implements SessionAware {
	private Map<String, Object> sessionMap;
	private User user = null;
	private String username,nom,prenom,email,password;
	private Civilite civilite=null;
	private UserStatus userStatus=null;
	private Role role=null;
	private Long id = null;
//	private UserService userService = new UserService();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserStatus getUserStatus() {
		return userStatus;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = Role.valueOf(role);
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = UserStatus.valueOf(userStatus);
	}

	private List<User> userList = new ArrayList<User>();

	public void setSession(Map<String, Object> arg0) {
		this.sessionMap = arg0;
	}
	
	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = Civilite.valueOf(civilite);
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	
	@Action(value="doCreateUser", results={@Result(name = "success", type = "json")})
	public String doCreateUser() throws IllegalArgumentException, IllegalAccessException{
		user = new User(username, password, nom, prenom, email, civilite);
		sessionMap.put("user", user);
//		userService.addUser(user);
		return SUCCESS;
	}

	@Action(value="doGetAllUsers", results={@Result(name = "success", type = "json")})
	public String doGetAllUsers() throws IllegalArgumentException, IllegalAccessException{
//		userList = userService.getAllUsers();
		return SUCCESS;
	}
	
	@Action(value="doUpdateUser", results={@Result(name = "success", type = "json")})
	public String doUpdateUser() throws IllegalArgumentException, IllegalAccessException{
//		user = userService.getUserById(id);
		user.setNom(nom);
		user.setCivilite(civilite);
		user.setEmail(email);
		user.setPassword(password);
		user.setPrenom(prenom);
		user.setUsername(username);
		user.setUserStatus(userStatus);
		user.setRole(role);
//		userService.updateUser(user);
		return SUCCESS;
	}

	@Action(value="doUpdateUserStatus", results={@Result(name = "success", type = "json")})
	public String doUpdateUserStatus() throws IllegalArgumentException, IllegalAccessException{
//		user = userService.getUserById(id);
		user.setUserStatus(userStatus);
//		userService.updateUser(user);
		return SUCCESS;
	}
	
}
	

