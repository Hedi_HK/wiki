package com.perou.wiki.controllers.struts;

import java.util.Map;

import com.perou.wiki.services.UserService;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.perou.wiki.beans.User;
import com.perou.wiki.interfaces.Login;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SuppressWarnings("serial")
@ParentPackage("json-default")
public class LoginAction extends ActionSupport implements SessionAware {
    //	private UserService userService = new UserService();
    private String username;
    private String password;
    private Map<String, Object> sessionMap;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSession(Map<String, Object> arg0) {
        this.sessionMap = arg0;

    }

    public Map<String, Object> getSessionMap() {
        return sessionMap;
    }

    @Action(value = "doLogin", results = {@Result(name = "success", type = "json")})
    public String doLogin() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext();
        UserService usx = (UserService) ctx.getBean("userService");
        User user = usx.findByUsername(username);
        System.out.println(username + " " + password);
        if (user != null && user.getPassword().equals(password))
            sessionMap.put("user", user);

        return SUCCESS;
    }

    @Action(value = "doLogout", results = {@Result(name = "success", type = "json")})
    public String doLogout() {
        System.out.println("LoginAction.doLogout()");
        sessionMap.clear();
        return SUCCESS;
    }

}
