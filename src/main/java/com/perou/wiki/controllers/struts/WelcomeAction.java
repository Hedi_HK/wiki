package com.perou.wiki.controllers.struts;

import java.util.Date;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.perou.wiki.beans.User;

@SuppressWarnings("serial")
@Action(value = "welcome")
@Result(name = "success", location = "/pages/accueil.jsp")
public class WelcomeAction extends ActionSupport implements SessionAware{
	private Map<String, Object> sessionMap;
	
	public String execute(){
		if (sessionMap.containsKey("user"))
			System.out.println(((User) sessionMap.get("user")).getUsername() +" " + "is connected at: " + new Date());
		else
			System.out.println("no one connected");
		
		return SUCCESS;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}



	

	
}
