package com.perou.wiki.controllers.struts;

import java.io.File;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.MultipartConfig;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.perou.wiki.beans.Article;
import com.perou.wiki.beans.ArticleStatus;
import com.perou.wiki.beans.User;

@MultipartConfig
@SuppressWarnings("serial")
@ParentPackage("json-default")
@InterceptorRefs({
    @InterceptorRef("fileUpload"),
    @InterceptorRef("defaultStack")
})
public class ArticleAction extends ActionSupport implements SessionAware{
	private Map<String, Object> sessionMap;
	private Article article = null;
	private String title, content, author;
	private Date datePublication;
	private Long score,id;
	private List<Article> articleList = null;
	private File file;
	private String fileContentType;
	private String fileFileName;
//	private ArticleService articleService = new ArticleService();
	private ArticleStatus articleStatus;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public void setSession(Map<String, Object> arg0) {
		this.sessionMap = arg0;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public ArticleStatus getArticleStatus() {
		return articleStatus;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getDatePublication() {
		return datePublication;
	}

	public void setDatePublication(Date datePublication) {
		this.datePublication = datePublication;
	}

	public Long getScore() {
		return score;
	}

	public void setScore(Long score) {
		this.score = score;
	}

	public void setArticleStatus(ArticleStatus articleStatus) {
		this.articleStatus = articleStatus;
	}
	
	public List<Article> getArticleList() {
		return articleList;
	}

	public void setArticleList(List<Article> articleList) {
		this.articleList = articleList;
	}

	@Action(value = "doCreateArticle", results = { @Result(name = "success", type = "json") })
	public String doCreateArticle() throws IllegalArgumentException, IllegalAccessException, SQLException {
//		articleService.addArticle(new Article(title, content, (User) sessionMap.get("user")));
		return SUCCESS;
	}

	@Action(value = "doGetAllArticles", results = { @Result(name = "success", type = "json") })
	public String doGetAllArticles() throws IllegalArgumentException, IllegalAccessException, SQLException {
//		articleList = articleService.getAllArticles();
		return SUCCESS;
	}

	@Action(value = "doGetArticleByTitle", results = { @Result(name = "success", type = "json") })
	public String doGetUserByTitle() throws IllegalArgumentException, IllegalAccessException, SQLException {
//		articleList = articleService.getArticlesByTitle(title);
		return SUCCESS;
	}
	
	@Action(value = "doGetArticleByContent", results = { @Result(name = "success", type = "json") })
	public String doGetUserByContent() throws IllegalArgumentException, IllegalAccessException, SQLException {
//		articleList = articleService.getArticlesByContent(content);
		return SUCCESS;
	}

	@Action(value = "doUpdateArticle", results = { @Result(name = "success", type = "json") })
	public String doUpdateArticle() throws IllegalArgumentException, IllegalAccessException, SQLException {
//		article = articleService.getArticleById(id);
		article.setContent(content);
		article.setArticleStatus(articleStatus);
		article.setTitle(title);
		article.setScore(score);
//		articleService.updateArticle(article);
		return SUCCESS;
	}
	
	@Action(value = "doCreateArticleFromXML", results = { @Result(name = "success", type = "json") })
	public String doCreateArticleFromXML() throws IllegalArgumentException, IllegalAccessException, SQLException {
		System.out.println(fileFileName);
		System.out.println(fileContentType);
		return SUCCESS;
	}


}
