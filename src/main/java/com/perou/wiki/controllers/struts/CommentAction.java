
package com.perou.wiki.controllers.struts;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.perou.wiki.beans.Article;
import com.perou.wiki.beans.Comment;

	@SuppressWarnings("serial")
	@ParentPackage("json-default")
	public class CommentAction extends ActionSupport implements SessionAware {
		
		
		private Map<String, Object> sessionMap;
		private Comment comment = null;
		private	TreeSet<String> responses;
		private String title, content, author;
		private Date datePublication;
		private int id, score, id_article;

		public void setSession(Map<String, Object> arg0) {
			this.sessionMap = arg0;
		}

		public Map<String, Object> getSessionMap() {
			return sessionMap;
		}

		
		public Comment getComment() {
			return comment;
		}

		public void setComment(Comment comment) {
			this.comment = comment;
		}

		public TreeSet<String> getResponses() {
			return responses;
		}

		public void setResponses(TreeSet<String> responses) {
			this.responses = responses;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public String getAuthor() {
			return author;
		}

		public void setAuthor(String author) {
			this.author = author;
		}

		public Date getDatePublication() {
			return datePublication;
		}

		public void setDatePublication(Date datePublication) {
			this.datePublication = datePublication;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public int getScore() {
			return score;
		}

		public void setScore(int score) {
			this.score = score;
		}

		public int getId_article() {
			return id_article;
		}

		public void setId_article(int id_article) {
			this.id_article = id_article;
		}

		@Action(value = "doCreateComment", results = { @Result(name = "success", type = "json") })
		public String doCreateComment() throws IllegalArgumentException, IllegalAccessException, SQLException {
			System.out.println(author+" "+content);
			return SUCCESS;

		}

		@Action(value = "doGetAllComments", results = { @Result(name = "success", type = "json") })
		public String doGetAllComments() throws IllegalArgumentException, IllegalAccessException, SQLException {
			return SUCCESS;
		}

		@Action(value = "doGetCommentByArticle", results = { @Result(name = "success", type = "json") })
		public String doGetCommentByArticle() throws IllegalArgumentException, IllegalAccessException, SQLException {
			return SUCCESS;
		}

		@Action(value = "doUpdateComment", results = { @Result(name = "success", type = "json") })
		public String doUpdateComment() throws IllegalArgumentException, IllegalAccessException, SQLException {
			// Recup ID de l'article puis le sous ID du commentaire
			comment.setId(id);
			
			// Creer le commentaire modified

			// Effectuer modif dans la base
			comment.setContent(content);
			comment.setDatePublication(datePublication);
			comment.setScore(score);


			return SUCCESS;
		}

	}


