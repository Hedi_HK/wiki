package com.perou.wiki.beans;

public enum UserStatus {
	ACTIVE,
	INACTIVE,
	SUSPENDED;
}
