package com.perou.wiki.beans;

public enum Role {

	ADMINISTRATEUR,
	CONTRIBUTEUR,
	AUTEUR;
}
