package com.perou.wiki.beans;

public enum ArticleStatus {
	VALIDATED,
	PENDING,
	REFUSED;

}
