package com.perou.wiki.beans;

import java.util.Date;
import java.util.List;
import java.util.TreeSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="comment")
public class Comment {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
//	@Column(name="comment_id")
//	private int idComment;
	@Column(name="content")
	private String content;
	@Column(name ="score")
	private int score;
	@Temporal(TemporalType.DATE)
	@Column(name="publication")
	Date datePublication;
	
	//Mapping de l user qui commente
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="user_id")
	User u;
	
	//Mapping de l article commente
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="article_id")
	Article a;
	
	//Sous-commentaires
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="comment_id")
	Comment c;
	@OneToMany(fetch = FetchType.EAGER,mappedBy="c")
	List<Comment> responses;
	
	
	
	public Comment() {
	}

	public Comment(int id, String content, int score, Article a, Date datePublication, Comment c) {
		this.id = id;
		this.content = content;
		this.score = 0;
		this.a = a;
		this.datePublication = datePublication;
		this.c = c;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	
	public Date getDatePublication() {
		return datePublication;
	}
	public void setDatePublication(Date date_publication) {
		this.datePublication = date_publication;
	}

	public Comment getC() {
		return c;
	}

	public void setC(Comment c) {
		this.c = c;
	}

	public List<Comment> getResponses() {
		return responses;
	}

	public void setResponses(List<Comment> responses) {
		this.responses = responses;
	}

	public User getU() {
		return u;
	}

	public void setU(User u) {
		this.u = u;
	}

	public Article getA() {
		return a;
	}

	public void setA(Article a) {
		this.a = a;
	}

}
