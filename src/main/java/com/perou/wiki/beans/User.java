package com.perou.wiki.beans;

import org.springframework.stereotype.Component;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Component("user")
@Entity
@NamedQuery(name="User.findByName",query="SELECT c FROM User c WHERE c.username = :username")
@Table(name = "user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "username")
	private String username;
	@Column(name = "password")
	private String password;
	@Column(name = "nom")
	private String nom;
	@Column(name = "prenom")
	private String prenom;
	@Column(name = "email")
	private String email;
	@Column(name = "civilite")
	@Enumerated(EnumType.STRING)
	private Civilite civilite;
	@Column(name = "role")
	@Enumerated(EnumType.STRING)
	private Role role;
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private UserStatus userStatus;
	@OneToMany(fetch = FetchType.EAGER,mappedBy="u")
	Set<Article> listArticles;
	@OneToMany(mappedBy="u")
	Set<Comment> listComment;
	

	public User() {
	}
	

	public User(String username, String password, String nom, String prenom, String email, Civilite civilite, Role role,
			UserStatus userStatus) {
		this.username = username;
		this.password = password;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.civilite = civilite;
		this.role = role;
		this.userStatus = userStatus;
	}



	@SuppressWarnings("static-access")
	public User(String username, String password, String nom, String prenom, String email, Civilite civilite) {
		this.username = username;
		this.password = password;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.civilite = civilite;
		this.role = Role.CONTRIBUTEUR;
		this.userStatus = userStatus.ACTIVE;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public UserStatus getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(UserStatus userStatus) {
		this.userStatus = userStatus;
	}


	public Set<Article> getListArticles() {
		return listArticles;
	}


	public void setListArticles(Set<Article> listArticles) {
		this.listArticles = listArticles;
	}
	
	

}
