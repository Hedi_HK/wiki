package com.perou.wiki.beans;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "article")
@NamedQueries({
	@NamedQuery(name="Article.findByContent",query="FROM Article a WHERE lower(a.content) like :content"),
	@NamedQuery(name="Article.findByTitle",query="FROM Article a WHERE lower(a.title) like :title")
	})
public class Article{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "title")
	private String title;
	@Column(name = "content")
	private String content;
	@Column(name = "publication")
	@Temporal(TemporalType.DATE)
	Date datePublication;
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private ArticleStatus articleStatus;
	@Column(name = "score")
	private Long score;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="author_id")
	User u;
	@OneToMany(fetch = FetchType.EAGER,mappedBy="a")
	Set<Comment> listComments;
	
	
	public Article() {
	}
	
	

	public Article(String title, String content, User u) {
		this.title = title;
		this.content = content;
		this.u = u;
		this.datePublication = new Date();
		this.articleStatus = ArticleStatus.PENDING;
		this.score = (long) 0;
	}



	protected Long getId() {
		return id;
	}

	protected void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}


	public User getU() {
		return u;
	}



	public void setU(User u) {
		this.u = u;
	}


	public Long getScore() {
		return score;
	}

	public void setScore(Long score) {
		this.score = score;
	}

	public Date getDatePublication() {
		return datePublication;
	}

	public void setDatePublication(Date datePublication) {
		this.datePublication = datePublication;
	}

	public ArticleStatus getArticleStatus() {
		return articleStatus;
	}

	public void setArticleStatus(ArticleStatus articleStatus) {
		this.articleStatus = articleStatus;
	}
}
